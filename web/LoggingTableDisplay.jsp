<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html>
<head>
    <title>Exercise 01</title>
</head>
<body>

<form action="./question1/new" method="post">
    Name:<br>
    <input type="text" name="name" value="name">
    <br>
    Description:<br>
    <input type="text" name="description" value="description">
    <br><br>
    <input type="submit" value="Submit">
</form>

<table border="1">


    <%--table header --%>
    <tr #e6ffe6>
        <th>Id</th>
        <th>Name</th>
        <th>Description</th>
        <th>Time</th>

    </tr>

    <%--forloop using the info from servlet(LoggingTable.java) and present in the table.--%>
    <%--table body--%>


    <c:forEach items="${AccessLogs}" var="log">

        <tr>
            <td bgcolor="#ffe6e6">
                <p>${log.getId()}</p>
            </td>

            <td bgcolor="#ffffe6">
                <p>${log.getName()}</p>
            </td>

            <td bgcolor=" #f2e6ff">
                <p>${log.getDescription()}</p>
            </td>

            <td bgcolor="#e6f7ff">
                <p>${log.getDate_registered()}</p>
            </td>
        </tr>

    </c:forEach>
    <c:if test="${ AccessLogs.size() >= 30}">
        <tr #e6ffe6>
            <th>Id</th>
            <th>Name</th>
            <th>Description</th>
            <th>Time</th>
        </tr>
    </c:if>


</table>






</body>
</html>
