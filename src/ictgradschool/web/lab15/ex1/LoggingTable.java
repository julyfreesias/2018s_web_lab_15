package ictgradschool.web.lab15.ex1;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class LoggingTable extends HttpServlet {
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // TODO: Retrieve LoggingTable entries and pass them to the LoggingTableDisplay.jsp file
        try {

            List<AccessLog> accessLogs = getAccessLogs();

            request.setAttribute("AccessLogs", accessLogs);

            request.getRequestDispatcher("LoggingTableDisplay.jsp").forward(request,response);

        } catch (SQLException ex) {

            throw new ServletException(ex);

        }

    }

    /**
     * Displays a list of articles as HTML.
     */
    private List<AccessLog> getAccessLogs() throws SQLException {

        try (Connection conn = DBConnection.createConnection()) {

            AccessLogDAO accessLogDAO = new AccessLogDAO(conn);
            List<AccessLog> accessLogs = accessLogDAO.getAccessLog();

            return accessLogs;
        }
    }

    }

