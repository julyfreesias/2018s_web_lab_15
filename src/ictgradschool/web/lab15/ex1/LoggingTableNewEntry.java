package ictgradschool.web.lab15.ex1;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.SQLException;
import java.util.List;

public class LoggingTableNewEntry extends HttpServlet {
//    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
//        // TODO: Retrieve parameters and store new entries in the database
//        // TODO: Redirect back to the LoggingTable
//
//        String title = request.getParameter("name");
//        String description = request.getParameter("description");
//
//
//        try {
//
//            List<AccessLog> accessLogs = getAccessLogs();
//
//            request.setAttribute("title", title);
//            request.setAttribute("description", description);
//
//            request.getRequestDispatcher("LoggingTableDisplay.jsp").forward(request,response);
//
//        } catch (SQLException ex) {
//
//            throw new ServletException(ex);
//
//        }
//
//    }


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setContentType("text/html");
        PrintWriter pw = response.getWriter();
        String connectionURL = "jdbc:mysql://db.sporadic.nz:3306/skwo116";// userdb is the database

        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }

        try (Connection connection = DriverManager.getConnection(connectionURL, "skwo116", "BowedDullAsiaticBlackBear")){

            String Username = request.getParameter("name");
            String description= request.getParameter("description");

            try (PreparedStatement pst = connection.prepareStatement("insert into access_log(name,description) values(?,?)")) {

                pst.setString(1,Username);
                pst.setString(2,description);

                int i = pst.executeUpdate();
                if(i!=0){
                    pw.println("<br>Record has been inserted");

                }
                else{
                    pw.println("failed to insert the data");
                }

            }

        }
        catch (Exception e){
            pw.println(e);
        }

        response.sendRedirect("../question1");
    }




    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.setStatus(HttpServletResponse.SC_METHOD_NOT_ALLOWED);
    }
}
