
CREATE TABLE IF NOT EXISTS access_log (
  id INT NOT NULL AUTO_INCREMENT,
  date_registered TIMESTAMP NOT NULL DEFAULT CURRENT_TIMESTAMP,
  name VARCHAR(64) NOT NULL,
  description VARCHAR(64) NOT NULL,
  PRIMARY KEY(id)
);

INSERT INTO access_log ( name,description)VALUES
  ("Soo","fgsdfg"),
  ("Goo","fgsdfg"),
  ("Eoo","fgsdfg");
