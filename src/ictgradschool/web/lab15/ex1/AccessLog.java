package ictgradschool.web.lab15.ex1;

import java.io.Serializable;
import java.sql.Timestamp;

public class AccessLog implements Serializable{
    private String name;
    private int id;
    private String description;
    private Timestamp date_registered;

    public AccessLog (){ }  //java bean should have empty constructor.

    //this constructor is for the variables we will grab from db
    public AccessLog (String name, int id, String description, Timestamp date_registered){
        this.name =name;
        this.id= id;
        this.description=description;
        this.date_registered=date_registered;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Timestamp getDate_registered() {
        return date_registered;
    }

    public void setDate_registered(Timestamp date_registered) {
        this.date_registered = date_registered;
    }
}
